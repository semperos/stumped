(ns user
  ^{:clj-kondo/config
    {:linters
     {:unused-namespace
      {:exclude ["clojure\\.repl"]}}}}
  (:require
   [clj-kondo.core :as kondo]
   [clojure.repl :refer [doc apropos]]
   [com.semperos.stumped :as stumped]
   [datascript.core :as d]))

(defn create-conn
  []
  (intern 'user 'conn (stumped/create-conn)))

(defn reset-conn
  []
  (create-conn))

(comment
  (def conn "clj-kondo")
  (->> (kondo/run! {:lint ["/Users/dlg/dev/clojure/stumped"]
                   :config {:output {:analysis true}}})
       :analysis
       #_(stumped/populate! conn))

  (d/q '[:find (count ?e) .
         :where
         [?e ?a ?v]]
       @conn)

  (reset-conn)
  )
