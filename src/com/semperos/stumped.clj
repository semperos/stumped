(ns com.semperos.stumped
  (:require
   [datascript.core :as d])
  (:import
   [java.util.concurrent.atomic AtomicLong]))

;; TODO Consider which can/should be ref's
;; TODO Consider making either def or usage shorter
;; TODO Consider how to make schema extensible to account for arbitrary metadata
(def clj-kondo-analysis-schema
  {;; :namespace-definitions ;;

   :namespace-definition/filename {}
   :namespace-definition/row {}
   :namespace-definition/col {}
   :namespace-definition/name {}
   :namespace-definition/lang {}
   :namespace-definition/deprecated {}
   :namespace-definition/doc {}
   :namespace-definition/author {}
   :namespace-definition/added {}
   :namespace-definition/no-doc {}

   ;; :namespace-usages ;;

   :namespace-usage/filename {}
   :namespace-usage/row {}
   :namespace-usage/col {}
   :namespace-usage/from {}
   :namespace-usage/to {}
   :namespace-usage/alias {}
   :namespace-usage/lang {}

   ;; :var-definitions ;;

   :var-definition/filename {}
   :var-definition/row {}
   :var-definition/col {}
   :var-definition/ns {}
   :var-definition/name {}
   :var-definition/fixed-arities {}
   :var-definition/varargs-min-arity {}
   :var-definition/private {}
   :var-definition/macro {}
   :var-definition/deprecated {}
   :var-definition/doc {}
   :var-definition/added {}
   :var-definition/test {}
   :var-definition/defined-by {}

   ;; :var-usages ;;
   :var-usage/filename {}
   :var-usage/row {}
   :var-usage/col {}
   :var-usage/name {}
   :var-usage/from {}
   :var-usage/to {}
   :var-usage/from-var {}
   :var-usage/arity {}
   :var-usage/private {}
   :var-usage/macro {}
   :var-usage/fixed-arities {}
   :var-usage/varargs-min-arity {}
   :var-usage/lang {}
   :var-usage/deprecated {}
   })

(def entities
  {:namespace-definitions :namespace-definition
   :namespace-usages :namespace-usage
   :var-definitions :var-definition
   :var-usages :var-usage})

(def entity-names
  (sort (distinct (map namespace (keys clj-kondo-analysis-schema)))))

(defn entity-attrs [entity-name]
  (sort (into []
              (filter (fn [[k _v]] (= (name entity-name) (namespace k))))
              clj-kondo-analysis-schema)))

(def ^:private db-id (AtomicLong. 0))
(defn next-db-id []
  (.incrementAndGet db-id))

(defn analysis-tx-data
  [analysis]
  (reduce-kv
   (fn [tx-data analysis-key analysis-data]
     (if-let [entity (get entities analysis-key)]
       (concat tx-data
               (sequence
                (comp
                 (map
                  (fn [analysis-datum]
                    (let [tx-data-map (reduce-kv
                                       (fn [tx-data-map k v]
                                         (if (nil? v)
                                           tx-data-map
                                           (assoc tx-data-map (keyword (name entity) (name k)) v)))
                                       {}
                                       analysis-datum)]
                      (when (seq tx-data-map)
                        ;; TODO Is this necessary? Doesn't appear to be.
                        (assoc tx-data-map :db/id (next-db-id))))))
                 (remove nil?))
                analysis-data))
       tx-data))
   []
   analysis))

(defn create-conn []
  (d/create-conn clj-kondo-analysis-schema))

(defn populate! [conn analysis]
  (let [tx-data (analysis-tx-data analysis)]
    (intern 'user 'tx-data)
    (d/transact! conn tx-data)))
