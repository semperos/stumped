(defproject com.semperos/stumped "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "https://gitlab.com/semperos/stumped"
  :license {:name "Mozilla Public License 2.0"
            :url "https://www.mozilla.org/en-US/MPL/2.0/"}
  :dependencies [[org.clojure/clojure "1.10.2-alpha2"]

                 ;; Alphabetical.
                 [clj-kondo "2020.10.10"]
                 [datascript "1.0.1"]
                 ])
