(ns com.semperos.stumped-test
  (:require [clojure.test :refer [deftest is]]
            [com.semperos.stumped :as nut]
            [com.semperos.stumped.test.data :as test-data]
            [datascript.core :as d]))

(deftest test-populate!
  (let [conn (nut/create-conn)
        _ (nut/populate! conn test-data/analysis-data)]
    (is (= (d/q '[:find (count ?e) .
                  :where
                  [?e _ _]]
                @conn)
           111))))
